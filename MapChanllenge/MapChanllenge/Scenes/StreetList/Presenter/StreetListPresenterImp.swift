//
//  StreetListPresenterImp.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//

import Foundation
import RxSwift

class StreetListPresenterImp {

    private var view: StreetListView? 
    var router: StreetListViewRouter?
    fileprivate var interactor: StreetListInteractor?
    fileprivate var dataSource: StreetListModel
    fileprivate let disposable = DisposeBag()
    fileprivate var keyword: String = ""
    
    init(view: StreetListView?,
         router: StreetListViewRouter?,
         interactor: StreetListInteractor?
    ) {
        self.view = view
        self.router = router
        self.interactor = interactor
        self.dataSource = StreetListModel()
    }
    
}

extension StreetListPresenterImp: StreetListPresenter {
    var searchValue: String { keyword }
    
    var sectionName: String {
        dataSource.pendingVisited > 0
            ? StreetListString.section.rawValue.replacingOccurrences(
                of: "##", with: "\(dataSource.pendingVisited)")
            : StreetListString.sectionEmpty.rawValue
    }
    var numberOfSections: Int { return 1 }
    
    var numberOfRows: Int { return dataSource.items.count }
    
    func getItem(at indexPath: IndexPath) -> StreetListItemModel? {
        guard dataSource.items.count > indexPath.row else { return nil }
        return dataSource.items[indexPath.row]
    }
    
    func didSelectedItem(at indexPath: IndexPath) {
        guard dataSource.items.count > indexPath.row,
              let identifier = dataSource.items[indexPath.row].id else { return }
        view?.loading(show: true)
        router?.presentStreetDetails(placeIdentifier: identifier)
    }
    
    func filterData(by keyword: String) {
        self.keyword = keyword
        view?.loading(show: true)
        interactor?
            .fetchPlaces(by: keyword)
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] places in
                self?.dataSource = places
                self?.view?.loading(show: false)
                self?.view?.reload()
            }, onError: { [weak self] error in
                self?.dataSource = StreetListModel()
                self?.view?.loading(show: false)
                self?.view?.reload()
            })
            .disposed(by: disposable)
    }
    
    func viewDidLoad(){
        //Initialize view
        filterData(by: keyword)
    }
}
