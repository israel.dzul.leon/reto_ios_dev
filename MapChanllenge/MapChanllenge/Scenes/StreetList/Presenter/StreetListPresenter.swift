//
//  
//  StreetListPresenter.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//
//
import Foundation

protocol StreetListPresenter: AnyObject {
    var router: StreetListViewRouter? {get}
    var numberOfSections:Int {get}
    var numberOfRows: Int {get}
    var sectionName: String {get}
    var searchValue: String {get}
    func viewDidLoad()
    func getItem(at indexPath: IndexPath) -> StreetListItemModel?
    func didSelectedItem(at indexPath: IndexPath)
    func filterData(by name: String)
    
    
}
