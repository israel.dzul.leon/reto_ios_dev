//
//  
//  StreetListModel.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//
//
import UIKit
import Foundation

struct StreetListModel : Equatable {
    static func == (lhs: StreetListModel, rhs: StreetListModel) -> Bool {
        return lhs.pendingVisited == rhs.pendingVisited
    }
    
    
    var pendingVisited : Int = 0
    var items: [StreetListItemModel] = []
    
}

struct StreetListItemModel: StreetViewIO {
    var id: Int64? = nil
    
    var status: String = "---"
    
    var title: String = "---"
    
    var subtitle: String = "---"
    
    var color: UIColor = .darkGray
    
    var isHideRightIcon: Bool = false
    
    var isHideButtons: Bool = true
    
    static func configue(with model: Place) -> StreetListItemModel {
        let coloredStatus: UIColor = (model.visited ? .visited : .notVisited)
        return StreetListItemModel(
            id: model.id,
            status: (model.visited
                        ? StreetListString.visited
                        : StreetListString.notVisited).rawValue ,
            title: model.street,
            subtitle: model.suburb,
            color: coloredStatus,
            isHideRightIcon: false,
            isHideButtons:  true)
    }
}

