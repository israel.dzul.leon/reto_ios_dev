//
//  
//  StreetListViewController.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//
//
import UIKit
import SVProgressHUD

class StreetListViewController: UITableViewController {

    var searchHeaderView: SearchHeaderFooterView?
    var presenter: StreetListPresenter?
    
    override func viewDidLoad(){
        super.viewDidLoad()
        configure()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.viewDidLoad()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let router = self.presenter?.router else {return}
        router.prepare(for: segue, sender: sender)
    }
    
    func configure(){
        StreetListBuilder
            .shared
            .configure(controller: self)
        tableView.registerNib(cell: StreetCell.self)
        navigationItem.rightBarButtonItem = .init(customView: .getLogoRight())
        navigationItem.leftBarButtonItem = .init(
            customView: .getGreetingUser(
                name: StreetListString.hi.rawValue,
                message: StreetListString.greetings.rawValue))
    }
    
    override func tableView(
        _ tableView: UITableView,
        viewForHeaderInSection section: Int) -> UIView? {
        let frame = CGRect(x: 0, y: 0, width: tableView.frame.width, height: 100)
        searchHeaderView = SearchHeaderFooterView();
        searchHeaderView?.frame = frame
        searchHeaderView?.sectionLabel.text
            = presenter?.sectionName ?? StreetListString.loading.rawValue
        searchHeaderView?.searchTextView.text = presenter?.searchValue ?? ""
        searchHeaderView?.delegate = self
        return searchHeaderView
    }
    
    override func tableView(
        _ tableView: UITableView,
        heightForHeaderInSection section: Int) -> CGFloat {
        return 135
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return presenter?.numberOfSections ?? 1
    }
    
    override func tableView(
        _ tableView: UITableView,
        numberOfRowsInSection section: Int) -> Int {
        return presenter?.numberOfRows ?? 0
    }
    
    override func tableView(
        _ tableView: UITableView,
        cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: StreetCell.self, for: indexPath)
        guard let item = presenter?.getItem(at: indexPath) else { return cell }
        cell.configureWith(obj: item)
        return cell;
    }
    
    override func tableView(
        _ tableView: UITableView,
        didSelectRowAt indexPath: IndexPath) {
        presenter?.didSelectedItem(at: indexPath)
    }
}

extension StreetListViewController: StreetListView {
    func loading(show:Bool){
        if show {
            SVProgressHUD.showNotDismiss()
            return
        }
        SVProgressHUD.dismiss()
    }
    
    func reload() {
        DispatchQueue.main.async { self.tableView.reloadData() }
    }
}


extension StreetListViewController: SearchHeaderFooterDelegate {
    func searchHeaderFooterView(
        view: SearchHeaderFooterView, didTextChange value: String) {
        presenter?.filterData(by: value)
    }
}
