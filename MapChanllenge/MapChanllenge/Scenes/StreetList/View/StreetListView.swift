//
//  
//  StreetListView.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//
//
import UIKit

enum StreetListString: String {
    case section = "Tienes ## visistas por hacer"
    case sectionEmpty = "No tienes mas lugares por visitar"
    case hi = "Hola israel"
    case greetings = ", buenos días"
    case visited = "Visitada"
    case notVisited = "Pendiente"
    case navIcon = "ic_nav_back"
    case loading = "Espere cargando información"
}

protocol StreetListView: AnyObject {
    func reload()
    func loading(show: Bool)
}
