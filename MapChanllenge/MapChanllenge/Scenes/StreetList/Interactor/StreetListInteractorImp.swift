//
//  StreetListInteractorImp.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//

import Foundation
import RxSwift

class StreetListInteractorImp {
    
    init() {
       
    }
    
}

extension StreetListInteractorImp: StreetListInteractor {
    
    func fetchPlaces(by keyword: String) -> Observable<StreetListModel> {
        
        Current.placeRepository()
            .placesFilterByStreet(keyword: keyword)
            .flatMap{ data -> Observable<StreetListModel> in
                Observable.just( StreetListModel(pendingVisited: data.0, items: self.mapping(data.1)))
            }
            .distinctUntilChanged()
            .share(replay: 1)
    }
    
    // MARK: - Mapping Observable Data
    
    private func  mapping(_ places: [Place]) -> [StreetListItemModel] {
        return places.map {
            place in StreetListItemModel.configue(with: place)
        }
    }

}
