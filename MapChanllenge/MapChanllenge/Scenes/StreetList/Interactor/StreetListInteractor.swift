//
//  
//  StreetListInteractor.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//
//
import RxSwift

protocol StreetListInteractor: AnyObject {
    func fetchPlaces(by keyword: String) -> Observable<StreetListModel>

}

