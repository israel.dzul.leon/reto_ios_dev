//
//  
//  StreetListViewRouter.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//
//
import UIKit


protocol StreetListViewRouter: ViewRouter {
    func presentStreetDetails(placeIdentifier: Int64)
}


class StreetListRouterDriver {
    
    fileprivate weak var controller: StreetListViewController?
    
    init(controller: StreetListViewController){
        self.controller = controller
    }
}

extension StreetListRouterDriver: StreetListViewRouter {
    func presentStreetDetails(placeIdentifier: Int64) {
        DispatchQueue.main.async {
            self.controller?.performSegue(
                withIdentifier: SegueCodes.StreetDetails.rawValue,
                sender: placeIdentifier)
        }
    }
    
    func prepare(
        for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueCodes.StreetDetails.rawValue,
           let identifier = sender as? Int64,
           let destination = segue.destination as? StreetDetailViewController {
            destination.identifier = identifier
        }
    }
}
