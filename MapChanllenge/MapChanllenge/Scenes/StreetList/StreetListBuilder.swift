//
//  
//  StreetListBuilder.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//
//
import UIKit

struct StreetListBuilder {

    /**
     singleton
     */
    static let shared = StreetListBuilder()
    private init(){}
    
    /**
     configure mvp
     */
    func configure(controller: StreetListViewController) {
        let interactor = StreetListInteractorImp()
        
        let router = StreetListRouterDriver(controller: controller)
        
        let presenter =  StreetListPresenterImp(
            view: controller,
            router: router,
            interactor: interactor)
        
        controller.presenter = presenter
    }

}
