//
//  
//  StreetDetailBuilder.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//
//
import UIKit

struct StreetDetailBuilder {

    /**
     singleton
     */
    static let shared = StreetDetailBuilder()
    private init(){}
    
    /**
     configure mvp
     */
    func configure(controller: StreetDetailViewController) {
        let interactor = StreetDetailInteractorImp()
        
        let router = StreetDetailRouterDriver(controller: controller)
        
        let presenter =  StreetDetailPresenterImp(
            view: controller,
            router: router,
            interactor: interactor)
        
        controller.presenter = presenter
    }

}
