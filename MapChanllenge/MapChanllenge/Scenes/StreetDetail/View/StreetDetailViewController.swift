//
//  
//  StreetDetailViewController.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//
//
import UIKit
import MapKit
import SVProgressHUD

class StreetDetailViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var placeInfoView: StreetView!
    
    var presenter: StreetDetailPresenter?
    var identifier: Int64?
    
    override func viewDidLoad(){
        super.viewDidLoad()
        configure()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let router = self.presenter?.router else {return}
        router.prepare(for: segue, sender: sender)
    }
        
    func configure(){
        StreetDetailBuilder
            .shared
            .configure(controller: self)
        mapView.delegate = self
        placeInfoView.delegate = self
        placeInfoView.isHidden = true
        presenter?.viewDidLoad()
    }
}

extension StreetDetailViewController: StreetDetailView {
    func updateView(by model: StreetDetailModel) {
        DispatchQueue.main.async {
            self.placeInfoView.isHidden = false
            self.placeInfoView.configureWith(obj: model)
            guard let location = model.location else { return }
            self.mapView.addAnnotation(
                PlacePointAnnotation(
                    location: location,
                    name: model.title,
                    isVisited: model.isVisited))
            self.mapView.setCenter(
                location, zoomLevel: 15, animated: true)
        }
    }
    
    func loading(show:Bool){
        DispatchQueue.main.async {
            if show {
                SVProgressHUD.showNotDismiss()
            } else {
                SVProgressHUD.dismiss()
            }
        }
    }
}

extension StreetDetailViewController: StreetViewDelegate {
    func streetViewNavigateClicked(view: StreetView) {
        
    }
    
    func streetViewCheckInClicked(view: StreetView) {
        presenter?.willRegisterCheckIn()
    }
}

extension StreetDetailViewController: MKMapViewDelegate {
    func mapView(
        _ mapView: MKMapView,
        viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        switch annotation {
        case let placeAnnotation as  PlacePointAnnotation:
            var pinView = self.mapView.dequeueReusableAnnotationView(
                withIdentifier: StreetDetailString.markerIdentifier.rawValue)
            if pinView == nil {
                pinView = MKPinAnnotationView(
                    annotation: annotation,
                    reuseIdentifier: StreetDetailString.markerIdentifier.rawValue)
                pinView?.isDraggable = false
                pinView?.canShowCallout = false
                pinView?.autoresizesSubviews = true
            }else {
                pinView?.annotation = annotation
            }
            pinView?.image = UIImage(
                named: (placeAnnotation.isVisited
                            ? StreetDetailString.visitedImg
                            : StreetDetailString.notVisitedImg).rawValue)
            return pinView
        default: return nil
        }
    }
}
