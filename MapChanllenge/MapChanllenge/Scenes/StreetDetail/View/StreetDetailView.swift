//
//  
//  StreetDetailView.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//
//
import UIKit
import MapKit

enum StreetDetailString: String {
    case visitedImg = "ic_visited_marker"
    case notVisitedImg = "ic_marker"
    case markerIdentifier = "placeMarkerIdentifier"
    case visited = "Visitada"
    case notVisited = "Pendiente"
}

protocol StreetDetailView: AnyObject {
    var identifier: Int64? {get set}
    func updateView(by model: StreetDetailModel)
    func loading(show:Bool)
}
