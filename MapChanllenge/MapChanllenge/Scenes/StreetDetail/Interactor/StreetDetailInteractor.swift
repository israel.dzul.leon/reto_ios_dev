//
//  
//  StreetDetailInteractor.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//
//
import RxSwift

protocol StreetDetailInteractor: AnyObject {
    func getPlaceDetails(
        by identifier: Int64) -> Observable<StreetDetailModel>
    
    func placeVisited(by identifier: Int64) -> Observable<Void>
}

