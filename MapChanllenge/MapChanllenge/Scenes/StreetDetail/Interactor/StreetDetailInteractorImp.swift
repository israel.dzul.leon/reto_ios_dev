//
//  StreetDetailInteractorImp.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//

import RxSwift

class StreetDetailInteractorImp {
    init() { }
}

extension StreetDetailInteractorImp: StreetDetailInteractor {
    
    func getPlaceDetails(
        by identifier: Int64) -> Observable<StreetDetailModel> {
        Current.placeRepository()
            .getPlace(by: identifier)
            .asObservable()
            .distinctUntilChanged()
            .flatMap{ place -> Observable<StreetDetailModel> in
                Observable.just(StreetDetailModel.configure(with: place))
            }
    }
    
    func placeVisited(by identifier: Int64) -> Observable<Void> {
        Current.placeRepository()
            .markAsVisited(by: identifier)
            .asObservable()
    }
    
}
