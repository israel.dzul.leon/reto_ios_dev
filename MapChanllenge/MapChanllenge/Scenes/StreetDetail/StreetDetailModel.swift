//
//  
//  StreetDetailModel.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//
//
import UIKit
import MapKit

struct StreetDetailModel: StreetViewIO {
    
    var status: String = "---"
    
    var title: String = "---"
    
    var subtitle: String = "---"
    
    var color: UIColor = .darkGray
    
    var isHideRightIcon: Bool = true
    
    var location: CLLocationCoordinate2D?
    
    var isVisited: Bool = false
    
    var isHideButtons: Bool = false
    
    static func configure(
        with model: Place?) -> StreetDetailModel {
        let coloredStatus: UIColor =
            (model?.visited ?? false ? .visited : .notVisited)
        return StreetDetailModel(
            status: (model?.visited ?? false
                        ? StreetDetailString.visited
                        : StreetDetailString.notVisited).rawValue ,
            title: model?.street ?? "---",
            subtitle: model?.suburb ?? "---",
            color: coloredStatus,
            isHideRightIcon: true,
            location: CLLocationCoordinate2D(
                latitude: model?.lat ?? 0,
                longitude: model?.lng ?? 0),
            isVisited: model?.visited ?? false,
            isHideButtons: model?.visited ?? false)
    }
}

