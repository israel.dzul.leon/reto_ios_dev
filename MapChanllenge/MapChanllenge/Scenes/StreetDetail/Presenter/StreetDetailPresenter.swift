//
//  
//  StreetDetailPresenter.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//
//
import Foundation


protocol StreetDetailPresenter: AnyObject {
    var router: StreetDetailViewRouter? {get}
    func viewDidLoad()
    func willRegisterCheckIn()
}

