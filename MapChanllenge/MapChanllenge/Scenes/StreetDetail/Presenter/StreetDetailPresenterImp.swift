//
//  StreetDetailPresenterImp.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//

import Foundation
import RxSwift

class StreetDetailPresenterImp {

    weak var view: StreetDetailView?
    var router: StreetDetailViewRouter?
    fileprivate var interactor: StreetDetailInteractor?

    fileprivate let disposable = DisposeBag()
    var loading: Bool = false
    
    init(view: StreetDetailView?,
         router: StreetDetailViewRouter?,
         interactor: StreetDetailInteractor?
    ) {
        self.view = view
        self.router = router
        self.interactor = interactor
    }
    
}

extension StreetDetailPresenterImp: StreetDetailPresenter {
    
    func willRegisterCheckIn() {
        guard let identifier = view?.identifier, identifier > 0 else {
            return
        }
        self.view?.loading(show: true)
        interactor?
            .placeVisited(by: identifier)
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] status in
                self?.view?.loading(show: false)
                self?.router?.presentStreetList()
            }, onError: { [weak self] error in
                self?.view?.loading(show: false)
            })
            .disposed(by: disposable)
    }
    
    func viewDidLoad(){
        if loading { return}
        loading = true
        self.view?.loading(show: true)
        interactor?
            .getPlaceDetails(by: view?.identifier ?? 0)
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] model in
                self?.view?.updateView(by: model)
                self?.loading = false
                self?.view?.loading(show: false)
            }, onError: { [weak self] error in
                self?.view?.updateView(by: StreetDetailModel())
                self?.loading = false
                self?.view?.loading(show: false)
            })
            .disposed(by: disposable)
    }
}
