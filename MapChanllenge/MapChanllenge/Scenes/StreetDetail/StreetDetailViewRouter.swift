//
//  
//  StreetDetailViewRouter.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//
//
import UIKit

protocol StreetDetailViewRouter: ViewRouter {
    func presentStreetList()
}


class StreetDetailRouterDriver {
        
    fileprivate weak var controller: StreetDetailViewController?
    
    init(controller: StreetDetailViewController){
        self.controller = controller
    }
}


extension StreetDetailRouterDriver: StreetDetailViewRouter{
    func presentStreetList() {
        controller?.navigationController?.popViewController(animated: true)
    }
    
    
}
