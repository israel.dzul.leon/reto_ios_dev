//
//  
//  FakeSplashViewRouter.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//
//
import UIKit

protocol FakeSplashViewRouter: ViewRouter {

    func presentStreetList()
}

class FakeSplashRouterDriver {
    
    fileprivate weak var controller: FakeSplashViewController?
    
    init(controller: FakeSplashViewController){
        self.controller = controller
    }
}

extension FakeSplashRouterDriver: FakeSplashViewRouter {
    
    func presentStreetList() {
        let storyBoard = UIStoryboard(storyboard: .main)
        let controller: MainNavControllerViewController = storyBoard.instantiateViewController()
        AppDelegate.setRootController(controller: controller)
    }
    
}
