//
//  
//  FakeSplashBuilder.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//
//
import UIKit

struct FakeSplashBuilder {

    static let shared = FakeSplashBuilder()
    private init(){}
    
    func configure(controller: FakeSplashViewController) {
        let interactor = FakeSplashInteractorImp()
        
        let router = FakeSplashRouterDriver(controller: controller)
        
        let presenter =  FakeSplashPresenterImp(
            view: controller,
            router: router,
            interactor: interactor)
        
        controller.presenter = presenter
    }

}
