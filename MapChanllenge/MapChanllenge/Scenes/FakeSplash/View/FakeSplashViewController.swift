//
//  
//  FakeSplashViewController.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//
//
import UIKit

class FakeSplashViewController: UIViewController {
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var retryButton: UIButton!
    @IBOutlet weak var continueButton: UIButton!
    
    var presenter: FakeSplashPresenter?
    
    override func viewDidLoad(){
        super.viewDidLoad()
        configure()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let router = self.presenter?.router else {return}
        router.prepare(for: segue, sender: sender)
    }
    
    @IBAction func didPressedSkipButton(_ sender: UIButton!) {
        presenter?.skipUpdate()
    }
    
    @IBAction func didPressedRetryButton(_ sender: UIButton!) {
        presenter?.retryNewData()
    }
    
    func configure(){
        FakeSplashBuilder
            .shared
            .configure(controller: self)
        presenter?.viewDidLoad()
    }
}

extension FakeSplashViewController: FakeSplashView {
    
    func update(mode ui: FakeSplashUIMode) {
        switch ui {
        case .SearchingNewData:
            messageLabel.text = FakeSplashString.checkNewData.rawValue
            retryButton.isHidden = true
            continueButton.isHidden = true
            activityIndicatorView.startAnimating()
        case .Failure:
            messageLabel.text = FakeSplashString.errorGetData.rawValue
            retryButton.isHidden = false
            continueButton.isHidden = false
            activityIndicatorView.stopAnimating()
            break
        }
    }
}
