//
//  
//  FakeSplashView.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//
//
import UIKit

enum FakeSplashString: String {
    case checkNewData = "Verificando si hay nueva información disponible, Espere un momento"
    case errorGetData = "Ha ocurrido un error al cargar la información, ¿Reintentar?"
    case retry = "Reintentar"
    case next = "Continuar"
}

enum FakeSplashUIMode {
    case SearchingNewData
    case Failure
}

protocol FakeSplashView: AnyObject {
    func update(mode ui: FakeSplashUIMode)
}
