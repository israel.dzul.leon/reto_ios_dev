//
//  
//  FakeSplashInteractor.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//
//
import Foundation
import RxSwift

class FakeSplashInteractorImp {
    init() {
       
    }
}

//MARK: - extension FakeSplashInteractor
extension FakeSplashInteractorImp: FakeSplashInteractor {
    
    func fetchStreetData() -> Observable<Bool> {
        return Current.placeRepository()
            .getCount()
            .asObservable()
            .distinctUntilChanged()
            .flatMap { count -> Observable<Bool> in
                //let currentDate = Date()
                //var needFetchDataFromApi = true
                //if count > 0 { needFetchDataFromApi = false}
                //if let dateStr = Preferences.get(
                //    key: PreferencesKey.DateLastUpdate) as? String,
                //   let storeDate = dateStr.toDate(),
                //   !currentDate.existDiffHour(to: storeDate) {
                //    needFetchDataFromApi = false
                //}
                
                if count > 0 { return Observable.just(true) }
                return NetworkManager
                    .sharedInstance
                    .execute(request: ApiRouter.Router.GetStreets)
                    .flatMap { result in  self.saveInDb(result)}
        }
    }
    
    private func saveInDb(_ result: Result<[PlaceBO]>) -> Observable<Bool> {
        switch result {
        case .success(let items):
            let data:[Place] = items.map { item in
                Place(
                    id: nil,
                    street: item.street ?? "",
                    suburb: item.suburb ?? "",
                    visited: item.isVisited ?? false,
                    lat: item.point?.lat ?? 0,
                    lng: item.point?.lng ?? 0)
            }
            return Current
                .placeRepository()
                .deleteAll()
                .asObservable()
                .flatMap { _  in
                    Current
                        .placeRepository()
                        .populate(with: data)
            }
        case .failure( _):
            return Observable.just(false)
        }
    }
    
}
