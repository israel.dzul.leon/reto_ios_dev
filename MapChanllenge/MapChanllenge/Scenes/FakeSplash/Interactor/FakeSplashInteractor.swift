//
//  IFakeSplashInteractor.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//

import Foundation
import RxSwift

protocol FakeSplashInteractor: AnyObject {
    func fetchStreetData() -> Observable<Bool>
}
