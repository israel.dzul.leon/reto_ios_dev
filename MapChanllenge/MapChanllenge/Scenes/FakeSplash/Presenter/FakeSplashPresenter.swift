//
//  FakeSplashPresenter.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//

import Foundation

protocol FakeSplashPresenter: AnyObject {
    var router: FakeSplashViewRouter? {get}
    func viewDidLoad()
    func retryNewData()
    func skipUpdate()
}
