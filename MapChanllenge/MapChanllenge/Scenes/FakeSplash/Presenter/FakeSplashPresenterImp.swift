//
//  
//  FakeSplashPresenter.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//
//
import Foundation
import RxSwift

class FakeSplashPresenterImp {

    weak var view: FakeSplashView?
    var router: FakeSplashViewRouter?
    fileprivate var interactor: FakeSplashInteractor?
    
    let disposable = DisposeBag()
    var isLoading: Bool = false
    
    init(view: FakeSplashView?,
         router: FakeSplashViewRouter?,
         interactor: FakeSplashInteractor?
    ) {
        self.view = view
        self.router = router
        self.interactor = interactor
    }
}

extension FakeSplashPresenterImp: FakeSplashPresenter {
    
    func skipUpdate() {
        router?.presentStreetList()
    }
    
    func retryNewData() {
        if isLoading { return }
        isLoading = true
        view?.update(mode: .SearchingNewData)
        
        interactor?
            .fetchStreetData()
            .observe(on: MainScheduler.instance)
            .subscribe(onNext: { [weak self] status in
                self?.isLoading = false
                guard status else {
                    let date = Date().toString()
                    Preferences.save(key: PreferencesKey.DateLastUpdate, data: date)
                    self?.view?.update(mode: .Failure)
                    return
                }
                self?.skipUpdate()
            }, onError: { [weak self] error in
                self?.view?.update(mode: .Failure)
                self?.isLoading = false
            })
            .disposed(by: disposable)
    }
    
    func viewDidLoad() {
        retryNewData()
    }
    
}
