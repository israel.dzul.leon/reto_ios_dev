//
//  AppLaucher.swift
//  AIDTelematics
//
//  Created by Israel Alejandro Dzul León on 17/09/20.
//  Copyright © 2020 telematics. All rights reserved.
//

import Foundation
import UIKit

class AppLauncher: NSObject {
    
    static let shared = AppLauncher()
    private override init() {}
    
    weak var window: UIWindow?
    
}
