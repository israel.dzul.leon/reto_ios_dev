//
//  BoxShadowRounded.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//

import UIKit

@IBDesignable
class BoxShadowRounded: UIView {
    
    @IBInspectable
    var opacityShadowBox: Float = 0.1 {
        didSet {
            configureBounds()
            layer.shadowOpacity = opacityShadowBox;
            //setNeedsDisplay()
        }
    }
    
    @IBInspectable
    var offsetShadowBox: CGSize = .zero {
        didSet {
            configureBounds()
            layer.shadowOffset = offsetShadowBox
            //setNeedsDisplay()
        }
    }
    
    @IBInspectable
    var radiusShadowBox: CGFloat = 6 {
        didSet {
            configureBounds()
            layer.shadowRadius = radiusShadowBox
            //setNeedsDisplay()
        }
    }
    
    @IBInspectable
    var colorShadowBox: UIColor = .black {
        didSet {
            configureBounds()
            layer.shadowColor = colorShadowBox.cgColor
            //setNeedsDisplay()
        }
    }
    
    @IBInspectable
    var cornerRadius: CGFloat = 14 {
        didSet {
            configureBounds()
            layer.cornerRadius = cornerRadius
            //setNeedsDisplay()
        }
    }
    
    func configureBounds() {
        self.clipsToBounds = true
        self.layer.masksToBounds = false
    }
    

}
