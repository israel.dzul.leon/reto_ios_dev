//
//  PlaceAnnotationView.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 18/07/21.
//

import MapKit

class PlacePointAnnotation: MKPointAnnotation {

    var isVisited: Bool = false
    var name: String = "---"
    
    init(location: CLLocationCoordinate2D,
         name: String,
         isVisited: Bool) {
        super.init()
        self.coordinate = location
        self.isVisited = isVisited
        self.name = name
    }

}
