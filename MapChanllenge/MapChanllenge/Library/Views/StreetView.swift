//
//  StreetView.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//

import UIKit

protocol StreetViewIO {
    var status: String {get}
    var title: String {get}
    var subtitle: String {get}
    var color: UIColor {get}
    var isHideRightIcon: Bool {get}
    var isHideButtons: Bool {get}
}

protocol StreetViewDelegate : AnyObject {
    func streetViewNavigateClicked(view: StreetView)
    func streetViewCheckInClicked(view: StreetView)
}

class StreetView: BoxShadowRounded {

    
    @IBOutlet weak var circelImageView: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var remarksLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var chekinButton: UIButton!
    @IBOutlet weak var navigateButton: UIButton!
    @IBOutlet weak var buttonsContainerStackView: UIStackView!
    
    private var model: StreetViewIO?
    weak var delegate: StreetViewDelegate?
    
    func configureWith(obj: StreetViewIO) {
        self.model = obj;
        render()
    }

    @IBAction func didPressedNavigateButton(_ sender: UIButton) {
        delegate?.streetViewNavigateClicked(view: self)
    }
    
    @IBAction func didPressedCheckInButton(_ sender: UIButton) {
        delegate?.streetViewCheckInClicked(view: self)
    }
    
    // loaded by storyboard
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        if self.subviews.count == 0 {
            xibSetup()
        }
    }
    
    // loaded programatly
    override init(frame: CGRect) {
        super.init(frame: frame)
        if self.subviews.count == 0 {
            xibSetup()
        }
    }
    
    //override func awakeFromNib() {
    //    super.awakeFromNib()
    //    render()
    //}
    
    func render () {
        guard let obj = model else {return}
        circelImageView.tintColor = obj.color
        statusLabel.textColor = obj.color
        statusLabel.text = obj.status
        nameLabel.text = obj.title
        remarksLabel.text = obj.subtitle
        iconImageView.isHidden = obj.isHideRightIcon
        navigateButton.isHidden = obj.isHideButtons
        chekinButton.isHidden = obj.isHideButtons
        buttonsContainerStackView.isHidden = obj.isHideButtons
    }

}
