//
//  SearchHeaderFooterView.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 18/07/21.
//

import UIKit

protocol SearchHeaderFooterDelegate: AnyObject {
    func searchHeaderFooterView(
        view: SearchHeaderFooterView, didTextChange value: String)
}

class SearchHeaderFooterView: UIView {
    @IBOutlet weak var searchTextView: UITextField!
    @IBOutlet weak var sectionLabel: UILabel!
    
    weak var delegate: SearchHeaderFooterDelegate?
    
    // loaded by storyboard
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        if self.subviews.count == 0 {
            xibSetup()
        }
    }
    
    // loaded programatly
    override init(frame: CGRect) {
        super.init(frame: frame)
        if self.subviews.count == 0 {
            xibSetup()
        }
    }
    
}


extension SearchHeaderFooterView: UITextFieldDelegate {
    
    func textField(
        _ textField: UITextField,
        shouldChangeCharactersIn range: NSRange,
        replacementString string: String) -> Bool {
        if string == "\n" {
            textField.resignFirstResponder()
            return false
        }
        
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(
                in: textRange, with: string)
            delegate?.searchHeaderFooterView(view: self, didTextChange: updatedText)
        }
        return true
    }
    
   
}
