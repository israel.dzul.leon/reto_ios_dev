//
//  StreetCell.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//

import UIKit



class StreetCell: UITableViewCell {
   
    @IBOutlet weak var streetView: StreetView!
    
    
    func configureWith(obj: StreetViewIO)  {
        streetView.configureWith(obj: obj)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
}
