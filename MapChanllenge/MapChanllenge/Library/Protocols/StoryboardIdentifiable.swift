//
//  StoryboardIdentifiable.swift
//
//  Created by Israel Alejandro Dzul León on 21/08/20.
//

import Foundation
import UIKit

protocol StoryboardIdentifiable {
    static var storyboardIdentifier: String { get }
}

extension StoryboardIdentifiable where Self: UIViewController {
    static var storyboardIdentifier: String {
        return String(describing: self)
    }
}
