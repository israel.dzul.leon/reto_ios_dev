//
//  SegueCodes.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//

import Foundation

enum SegueCodes: String {
    case StreetList = "StreetListSeague"
    case StreetDetails = "StreetDetailSegue"
}
