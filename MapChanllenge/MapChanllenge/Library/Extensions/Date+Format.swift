//
//  Date+Format.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 20/07/21.
//

import Foundation

extension String {

    func toDate(withFormat format: String = "yyyy-MM-dd' 'HH:mm:ssZ")-> Date?{
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from: self)
        return date
    }
}

extension Date {

    func toString(withFormat format: String = "yyyy-MM-dd' 'HH:mm:ssZ") -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let str = dateFormatter.string(from: self)
        return str
    }
    
    func existDiffHour(to date: Date ) -> Bool {
        let diff = Int(self.timeIntervalSince1970 - date.timeIntervalSince1970)
        let hours = diff / 3600
        //let minutes = (diff - hour * 3600) / 60
        return hours > 1
    }
}
