//
//  ProgresHUD+present.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 19/07/21.
//

import SVProgressHUD

extension SVProgressHUD {
    class func showNotDismiss(){
        SVProgressHUD.setBackgroundColor(.black)
        SVProgressHUD.setBackgroundLayerColor(.black)
        SVProgressHUD.setDefaultStyle(.light)
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.show()
    }
    
}
