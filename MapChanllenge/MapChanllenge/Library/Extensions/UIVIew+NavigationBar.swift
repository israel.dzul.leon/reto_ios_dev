//
//  UIVIew+NavigationBar.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 19/07/21.
//

import UIKit

extension UIView {
    class func getLogoRight() -> UIView {
        let containView = UIView(frame: CGRect(x: 0, y: 0, width: 90, height: 44))
        let imageview = UIImageView(frame: CGRect(x: 0, y: 0, width: 90, height: 44))
        imageview.image = UIImage(named: "avatar")
        imageview.contentMode = UIView.ContentMode.scaleAspectFit
        imageview.layer.cornerRadius = 20
        imageview.layer.masksToBounds = true
        containView.addSubview(imageview)
        return containView
    }
    
    class func getGreetingUser(name: String,  message: String) -> UIView{
        let text = "\(name)\(message)"
        let containView = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 44))
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 44))
        
        let attributedString = NSMutableAttributedString(string: text)
        let nameAttr: [NSAttributedString.Key: Any] = [
            .foregroundColor: UIColor.black,
            .font: UIFont.init(name: "Roboto-Medium", size: 17)!]
        let messageAttr: [NSAttributedString.Key: Any] = [
            .foregroundColor: UIColor.darkGray,
            .font: UIFont.init(name: "Roboto-Regular", size: 15)!]
        
        attributedString.addAttributes(
            nameAttr, range: NSRange(location: 0, length: name.count))
        attributedString.addAttributes(
            messageAttr, range: NSRange(location: name.count, length: message.count))
        
        label.attributedText = attributedString
        containView.addSubview(label)
        return containView
    }
    
    class func getBackButton() -> UIImage {
        return  UIImage(named: "ic_nav_back")!
    }
}
