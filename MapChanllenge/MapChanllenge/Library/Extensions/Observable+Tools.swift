//
//  RxAlamoFile+ExpectingObg.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//

import Foundation
import RxSwift

extension Observable where Element == (HTTPURLResponse, Data){
    
    func expectingObject<T : Decodable>(
        ofType type: T.Type
    ) -> Observable<Result<T>>{
        return self.map{ (httpURLResponse, data) -> Result<T> in
            switch httpURLResponse.statusCode {
            case 200 ... 299:
                do {
                    let object = try JSONDecoder().decode(type, from: data)
                    return .success(object)
                } catch {
                    let networkError = NetworkRequestError(error: error)
                    return .failure(networkError)
                }
            default:
                let apiError = ApiError(data: data, httpUrlResponse: httpURLResponse)
                return .failure(apiError)
            }
        }
    }
}
