//
//  String+Url.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//

import Foundation

extension String {
    public var url:URL? {
        return URL(string: self)
    }
}

extension URL: ExpressibleByStringLiteral {
    
    public init(stringLiteral value: StringLiteralType) {
        guard let url = URL(string: value) else {
            fatalError("cannot be convert to url object")
        }
        self = url
    }
        
    public init(extendedGraphemeClusterLiteral value: String) {
        self.init(stringLiteral: value)
    }

    public init(unicodeScalarLiteral value: String) {
       self.init(stringLiteral: value)
    }
}
    
