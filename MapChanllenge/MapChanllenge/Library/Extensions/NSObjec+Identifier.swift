//
//  NSObjec+tIdentifier.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//

import Foundation

extension NSObject {
    func reuseIdentifier() -> String {
        return String(describing: type(of: self))
    }
    
    class var reuseIdentifier: String {
        return String(describing: self)
    }
}
