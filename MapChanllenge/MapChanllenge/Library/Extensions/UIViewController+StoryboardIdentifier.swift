//
//  UIViewController+StoryboardIdentifier.swift
//
//  Created by Israel Alejandro Dzul León on 21/08/20.
//

import UIKit

extension UIViewController : StoryboardIdentifiable { }
