//
//  AppDelegate+RootController.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 18/07/21.
//

import UIKit

extension AppDelegate {
    class func setRootController(controller: UIViewController?) {
        guard let _controller = controller,
            let window = UIApplication.shared.windows.first else {
            return
        }
        
        window.rootViewController = _controller
        window.makeKeyAndVisible()
        // A mask of options indicating how you want to perform the animations.
        let options: UIView.AnimationOptions = .transitionCrossDissolve

        // The duration of the transition animation, measured in seconds.
        let duration: TimeInterval = 0.3
        
        UIView.transition(
            with: window,
            duration: duration,
            options: options,
            animations: {},
            completion: { completed in
            // maybe do something on completion here
        })
    }
    
    
    class func getRootViewController() -> UIViewController?{
        guard let window = UIApplication.shared.windows.first else {
            return nil
        }
        return window.rootViewController
    }
}
