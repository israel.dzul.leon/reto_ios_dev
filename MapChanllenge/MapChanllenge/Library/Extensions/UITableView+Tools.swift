//
//  UITableViewController+Register.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//

import UIKit

extension UITableView {
    
    func registerNib<T: UITableViewCell>(cell: T.Type) {
        register(
            UINib(nibName: T.reuseIdentifier, bundle: nil),
            forCellReuseIdentifier: T.reuseIdentifier
        )
    }
    
    func register<T: UITableViewCell>(cell: T.Type) {
        register(T.self, forCellReuseIdentifier: T.reuseIdentifier)
    }
    
    func registerNib<T: UITableViewHeaderFooterView>(headerFooterView: T.Type) {
        register(UINib(nibName: T.reuseIdentifier, bundle: nil),
                 forHeaderFooterViewReuseIdentifier: T.reuseIdentifier
        )
    }
    
    func register<T: UITableViewHeaderFooterView>(headerFooterView: T.Type) {
        register(T.self, forHeaderFooterViewReuseIdentifier: T.reuseIdentifier)
    }
    
    func dequeueReusableCell<T: UITableViewCell>(
        for type: T.Type,
        for indexPath: IndexPath
    ) -> T {
        guard let cell = dequeueReusableCell(
                withIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
            fatalError("failed to dequeue cell")
        }
        return cell
    }
    
    public func dequeueReusableHeaderFooterView<T: UITableViewHeaderFooterView>(
        for type: T.Type
    ) -> T {
        guard let view = dequeueReusableHeaderFooterView(
                withIdentifier: T.reuseIdentifier) as? T else {
            fatalError("Failed to dequeue header-footer view.")
        }
        return view
    }
}
