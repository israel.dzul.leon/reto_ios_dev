//
//  ApiError.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//

import Foundation

struct ApiError: Error {
    let data: Data?
    let httpUrlResponse: HTTPURLResponse
}
