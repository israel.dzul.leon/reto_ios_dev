//
//  PlaceBO.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//

import Foundation

struct PlaceBO : Codable {
    var street: String?
    var suburb: String?
    var isVisited: Bool? = false
    var point: LocationBO?
    
    enum CodingKeys: String, CodingKey {
        case street = "streetName"
        case suburb = "suburb"
        case isVisited = "visited"
        case point = "location"
    }
}
