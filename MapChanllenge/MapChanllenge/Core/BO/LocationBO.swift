//
//  LocationBO.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//

import Foundation

struct LocationBO: Codable {
    var lat: Double?
    var lng: Double?
    
    enum CodingKeys: String, CodingKey {
        case lat = "latitude"
        case lng = "longitude"
    }
}
