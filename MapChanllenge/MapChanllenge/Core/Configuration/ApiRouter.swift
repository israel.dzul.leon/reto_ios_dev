//
//  ApiRouter.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//

import Foundation
import Alamofire

struct ApiRouter {
    enum Router: URLRequestConvertible {
        
        case GetStreets
        
        public func asURLRequest() throws -> URLRequest {
            let result: (
                path: String,
                headers: [String: String]?,
                parameters: [String: AnyObject]?,
                method: HTTPMethod,
                encoding: Alamofire.ParameterEncoding
            ) = {
                switch self {
                case .GetStreets:
                    let URL = GlobalKeys.apiEndpoint
                    let method = HTTPMethod.get
                    let encoding = Alamofire.URLEncoding.default
                    return (URL, [:], [:], method, encoding)
                }
            }()
            
            var request = URLRequest(url: result.path.url!)
            request.httpMethod = result.method.rawValue
            request.timeoutInterval = TimeInterval(60)
            
            if let _header = result.headers {
                for key in Array(_header.keys) {
                    request.addValue(_header[key]!, forHTTPHeaderField: key)
                }
            }
            return try result.encoding.encode(request, with: result.parameters)
        }
    }
}
