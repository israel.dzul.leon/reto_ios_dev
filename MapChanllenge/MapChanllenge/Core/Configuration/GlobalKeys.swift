//
//  GlobalKeys.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//

import Foundation

struct GlobalKeys {
    
    static let apiEndpoint =
        "https://fintecimal-test.herokuapp.com/api/interview"
    
    static let dbName = "db.sqlite"
    
    static let dbVersion = "v1.0"
}
