//
//  NetworkManager.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//

import Foundation
import Alamofire
import RxAlamofire
import RxSwift

class NetworkManager {
    static let sharedInstance = NetworkManager()
    private init() {}
    
    let manager: Alamofire.Session = {
        let configuration = URLSessionConfiguration.default
        return Alamofire.Session(configuration: configuration)
    }()
    
    @discardableResult
    func execute<T: Decodable> (
        request: URLRequestConvertible) -> Observable<Result<T>> {
        return self.manager
            .rx
            .request(urlRequest: request)
            .validate(statusCode: 200...299)
            .responseData()
            .expectingObject(ofType: T.self)
    }
}
