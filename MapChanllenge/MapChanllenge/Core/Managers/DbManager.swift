//
//  AppDatabase.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 18/07/21.
//

import Foundation
import GRDB

struct DbManager {
    
    public static func configure (_ application: UIApplication) {
        do {
            // Create a DatabasePool for efficient multi-threading
            let databaseURL = try FileManager.default
                .url(
                    for: .applicationSupportDirectory,
                    in: .userDomainMask,
                    appropriateFor: nil,
                    create: true)
                .appendingPathComponent(GlobalKeys.dbName)
                
            let dbPool = try DatabasePool(path: databaseURL.path)
                
            // Setup the database
            try DbManager().setup(dbPool)
            Current = DbContext(database: { dbPool })
            
            // Maybe use this section for populate demo data.
            
        } catch {
            fatalError("error init database")
        }
    }
    
    
    /// Prepares a fully initialized database at path
    func setup(_ database: DatabaseWriter) throws {
        // Use DatabaseMigrator to define the database schema
        // See https://github.com/groue/GRDB.swift/#migrations
        try migrator.migrate(database)
        // Other possible setup include: custom functions, collations,
        // full-text tokenizers, etc.
    }
       
       
    /// The DatabaseMigrator that defines the database schema.
    // See https://github.com/groue/GRDB.swift/#migrations
    private var migrator: DatabaseMigrator {
        var migrator = DatabaseMigrator()
           
        #if DEBUG
        // Speed up development by nuking the database when migrations change
        migrator.eraseDatabaseOnSchemaChange = true
        #endif
           
        migrator.registerMigration(GlobalKeys.dbVersion) { db in
            try db.create(table: "place") { t in
                t.autoIncrementedPrimaryKey("id")
                t.column("street", .text)
                    .notNull()
                    .collate(.localizedCaseInsensitiveCompare)
                    .unique(onConflict: .replace)
                t.column("suburb", .text)
                    .notNull()
                t.column("visited", .boolean)
                    .notNull()
                t.column("lat", .double)
                    .notNull()
                t.column("lng", .double)
                    .notNull()
            }
        }
        
        return migrator
    }
}
