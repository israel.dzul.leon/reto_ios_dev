//
//  Result.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//

import Foundation

typealias Result<T> = Swift.Result<T, Error>
