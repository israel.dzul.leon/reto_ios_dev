//
//  Place.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 18/07/21.
//

import GRDB

// A player
struct Place: Codable, Equatable {
    var id: Int64?
    var street: String
    var suburb: String
    var visited: Bool
    var lat: Double
    var lng: Double
}

// Implementation is automatically derived from Codable.
extension Place: FetchableRecord { }

// database. Implementation is partially derived from Codable.
extension Place: MutablePersistableRecord {
    // Update auto-incremented id upon successful insertion
    mutating func didInsert(with rowID: Int64, for column: String?) {
        id = rowID
    }
}

// Define columns that we can use for our database requests.
// They are derived from the CodingKeys enum for extra safety.
extension Place {
    fileprivate enum Columns {
        static let id = Column(CodingKeys.id)
        static let street = Column(CodingKeys.street)
        static let suburb = Column(CodingKeys.suburb)
        static let visited = Column(CodingKeys.visited)
        static let lat = Column(CodingKeys.lat)
        static let lng = Column(CodingKeys.lng)
    }
}

// DerivableRequest protocol.
extension DerivableRequest where RowDecoder == Place {
    func orderByVisited() -> Self {
        return order(Place.Columns.visited.desc, Place.Columns.street)
    }
    
    func orderByStreet() -> Self {
        return order(Place.Columns.visited, Place.Columns.street.desc)
    }
}
