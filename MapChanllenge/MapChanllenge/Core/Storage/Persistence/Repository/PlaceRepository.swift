//
//  PlaceRepository.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 18/07/21.
//

import GRDB
import RxGRDB
import RxSwift

struct PlaceRepository {
    
    private let database: DatabaseWriter
    
    init(database: DatabaseWriter) {
        self.database = database
    }
    
    // MARK: - Modify Places
    @discardableResult
    func populate(with items: [Place]) -> Single<Bool>  {
        database.rx.write { database throws in
            for var item in items {
                try item.insert(database)
            }
            return true
        }
    }
    
    func deleteAll() -> Single<Void> {
        database.rx.write { database in
            try Place.deleteAll(database)
        }
    }
    
    func markAsVisited(by identifier: Int64) -> Single<Void>{
        database.rx.write { database throws in
            if var place = try Place.fetchOne(database, key: identifier) {
                place.visited = true
                try place.update(database)
            }
        }
    }
    
    func getPlace(by identifier: Int64) -> Single<Place?>{
        database.rx.read { database  throws in
            try Place.fetchOne(database, key: identifier)
        }
    }
    
    func getCount() -> Single<Int> {
        database.rx.read { database throws in
            try Place.fetchCount(database)
        }
    }
    
    func placesFilterByStreet(keyword: String) -> Observable<(Int, [Place])> {
        database.rx.read { database throws in
            let items = try Place
                .filter(Column("street").like("%\(keyword)%"))
                .order(Column("visited").desc)
                .fetchAll(database)
            let count = try Int.fetchOne(
                database,
                sql: "SELECT COUNT(*) FROM place WHERE visited == False")
            return (count ?? 0, items)
        }.asObservable()
    }
    
}
