//
//  PreferenceHelper.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//

import Foundation

struct Preferences {
    static func save<T>(key: PreferencesKey, data: T) -> Void {
        UserDefaults.standard.set(data, forKey: key.value())
        self.commit()
    }
    
    static func get(key: PreferencesKey) -> Any? {
        return UserDefaults.standard.object(forKey: key.value())
    }
    
    static func delete(key: PreferencesKey) {
        UserDefaults.standard.removeObject(forKey: key.value())
        self.commit()
    }
    
    static  func commit() {
        UserDefaults.standard.synchronize()
    }
}
