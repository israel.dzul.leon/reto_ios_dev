//
//  StorageKey.swift
//  MapChanllenge
//
//  Created by Israel Alejandro Dzul León on 17/07/21.
//

import Foundation
enum PreferencesKey: String {
    
    case DateLastUpdate = "DATE_LAST_UPDATE"
    
    func value() -> String {
        return self.rawValue;
    }
}
