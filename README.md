# reto_ios_dev

Proyecto  MapChallenge
Este proyecto esta escrito en swift 5.4 haciendo uso de storyboards.

Dependencias:
- RxSwfit
- RxAlamofire
- RxGRDB
- SVProgressHud

Para poder instalar los paquetes hay que correr.

``` bash
pod install
```

 @Author israel.dzul.leon@gmail.com
